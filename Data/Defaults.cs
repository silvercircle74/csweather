/**
 * Copyright (c) 2012-2024 Alex Vie (as@subspace.cc)
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
namespace fetchweather
{
  public class Defaults
  {
    public record ProviderInfo
    {
      public string Name { get; internal set; }
      public string Provider { get; internal set; }
      public string ApiKey { get; internal set; }
      public string Url { get; internal set; }
      public string FCUrl { get; internal set; }
      public string Location { get; internal set; }
    }
    private static readonly ProviderInfo[] pInfo = new ProviderInfo[] {
      new ProviderInfo {
        Name = "OWM",
        Provider = "OpenWeatherMap",
        ApiKey = "",
        Url = "http://api.openweathermap.org/data/2.5/onecall?appid=",
        FCUrl = "",
        Location = ""
      },
      new ProviderInfo {
        Name = "OWM3",
        Provider = "OpenWeatherMap Version 3",
        ApiKey = "",
        Url = "http://api.openweathermap.org/data/2.5/onecall?appid=",
        FCUrl = "",
        Location = ""
      },
      new ProviderInfo {
        Name = "VC",
        Provider = "Visual Crossing",
        ApiKey = "",
        Url = "https://weather.visualcrossing.com/VisualCrossingWebServices/rest/services/timeline/{0}?unitGroup=metric&include=events,days,hours,alerts,current&iconSet=icons2&contentType=json&key={1}",
        FCUrl = "",
        Location = ""
      },
      new ProviderInfo {
        Name = "CC",
        Provider = "Tomorrow IO, formerly ClimaCell",
        Url = "https://data.climacell.co/v4/timelines?&apikey=",
        ApiKey ="",
        FCUrl = "",
        Location = ""
      }
    };
    public static readonly string[] supportedApis = { "OWM", "VC", "CC", "OWM3" };

    public static ProviderInfo[] getKnownApis()
    {
      return pInfo;
    }
  }
}