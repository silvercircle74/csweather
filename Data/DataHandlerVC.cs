/**
 * Copyright (c) 2012-2024 Alex Vie (as@subspace.cc)
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
using Serilog;
using Serilog.Context;
using System.Text.Json;
using Tommy;

namespace fetchweather.Data
{
  /**
   * implements the Visual Crossing API. This is a single call API that
   * provides a lot of information. Current conditions and a detailed
   * forecast for several days.
   */
  public class DataHandlerVC : DataHandler {
    public DataHandlerVC() : base("VC")
    {
      TomlTable id = ctx.iniData;
      apiKey = ctx.Options.ApiKey != "nokey" ? ctx.Options.ApiKey : id[apiProvider]["apikey"];
      string baseurl = id[apiProvider]["baseurl"].ToString() ?? "";
      finalUrl = string.Format(baseurl, getLocation(), apiKey);
    }

    /**
     * <summary>
     * fetches JSON data and updates jsonDocument. If --offline option was given, try 
     * the cache and bail when it fails. Otherwise, fetch from the API and update the
     * cache.
     * </summary>
     * <returns>bool task success status</returns>
     */
    public override async Task<bool> Fetch()
    {
      bool rcq = await readAndCacheSolarData();
      bool tryCache = false;

      LogContext.PushProperty("ctx", "DataHandlerVC().Fetch()");
      bool TryCache()
      {
        this.jsonDocument = readJsonFromCache(out bool result, tag: "current");
        if (this.jsonDocument != null && result == true) {
          Populate();
          return true;
        }
        return false;
      }

      if (ctx.Options.Offline) {
        return TryCache();
      }

      try {
        using HttpResponseMessage r = await ctx.httpClient.GetAsync(finalUrl);
        r.EnsureSuccessStatusCode();
        string jsonResult = await r.Content.ReadAsStringAsync();
        jsonDocument = JsonDocument.Parse(jsonResult);
        // write to cache
        Log.Information("Fetch from VC API successful");
        ctx.CallStats.updateStats("VC", amount: 1, bytes: jsonResult.Length);
        if (Validate() == true) {
          Populate();
          AddHistory();
          if (p.valid == true) {
            cacheResult(jsonDocument, tag: "current");
          }
          return true;
        }
        tryCache = ctx.Options.TryCache;
      } catch (HttpRequestException e) {
        Log.Warning(e.Message);
        tryCache = ctx.Options.TryCache;
      }
      if (tryCache) {
        return TryCache();
      }
      return false;
    }

    /**
     * <summary>
     * get the single-character icon code corresponding to the weather font
     * symbol
     * </summary>
     * <param name="icon_code">API short description of the weather condition</param>
     * <returns>character for the font icon</returns>
     */
    protected char getIcon(string icon_code)
    {
      return icons.TryGetValue(icon_code, out char value) ? value : 'a';
    }

    protected override bool Validate()
    {
      bool success = true;
      LogContext.PushProperty("ctx", "DataHandlerOWM.Validate()");
      try {
        JsonElement current = this.jsonDocument.RootElement.GetProperty("currentConditions");
        JsonElement root = this.jsonDocument.RootElement;
        JsonElement today = this.jsonDocument.RootElement.GetProperty("days")[0];

        success &= current.GetProperty("temp").TryGetDouble(out double dummy);
        success &= today.GetProperty("tempmax").TryGetDouble(out dummy);
        current.GetProperty("conditions").ToString();
      } catch (Exception e) {
        Log.Error("Validation failed");
        Log.Error(e.Message);
        success = false;
      }
      return success;
    }

    /**
     * <summary>
     * Populate the DataPoint record from the json document.
     * </summary>
     */
    protected override void Populate()
    {
      LogContext.PushProperty("ctx", "DataHandlerVC.Populate()");
      JsonElement current = this.jsonDocument.RootElement.GetProperty("currentConditions");
      JsonElement root = this.jsonDocument.RootElement;
      JsonElement today = this.jsonDocument.RootElement.GetProperty("days")[0];
      try {
        p.temperature = current.GetProperty("temp").GetDouble();
        p.Api = this.apiProvider;
        if (solarData.valid) {
          p.sunriseTime = solarData.sunriseTime;
          p.sunsetTime = solarData.sunsetTime;
        } else {
          p.sunriseTime = DateTime.Parse(current.GetProperty("sunrise").ToString());
          p.sunsetTime = DateTime.Parse(current.GetProperty("sunset").ToString());
        }
        p.sunriseTimeAsString = p.sunriseTime.ToString("HH:mm");
        p.sunsetTimeAsString = p.sunsetTime.ToString("HH:mm");
        p.timeRecorded = DateTime.Parse(current.GetProperty("datetime").ToString());
        p.timeRecordedAsText = p.timeRecorded.ToString("HH:mm");
        p.weatherCode = 0;
        p.is_day = p.timeRecorded > p.sunriseTime && p.timeRecorded < p.sunsetTime;
        p.timeZone = root.GetProperty("timezone").ToString();
        p.conditionAsString = current.GetProperty("conditions").ToString();
        p.temperature = current.GetProperty("temp").GetDouble();
        p.temperatureApparent = current.GetProperty("feelslike").GetDouble();
        p.weatherSymbol = getIcon(current.GetProperty("icon").ToString());
        p.dewPoint = current.GetProperty("dew").GetDouble();
        p.temperatureMax = today.GetProperty("tempmax").GetDouble();
        p.temperatureMin = today.GetProperty("tempmin").GetDouble();

        p.visibility = convertVis(current.GetProperty("visibility").GetDouble(), source_unit: "km");
        p.pressureSeaLevel = convertPressure(current.GetProperty("pressure").GetDouble());
        p.humidity = current.GetProperty("humidity").GetDouble();
        p.uvIndex = current.GetProperty("uvindex").GetDouble();

        // precip
        p.precipitationProbability = current.GetProperty("precipprob").GetDouble();
        p.precipitationIntensity = current.GetProperty("precip").GetDouble();
        try {
          JsonElement preciptype = current.GetProperty("preciptype")[0];
          p.precipitationType = 1;
          p.precipitationTypeAsString = preciptype.ToString();
        } catch (Exception e) when (e is KeyNotFoundException or InvalidOperationException) {
          p.precipitationType = 0;
          p.precipitationIntensity = 0;
          p.precipitationTypeAsString = "";
        }

        // wind
        p.windDirection = (int)current.GetProperty("winddir").GetDouble();
        p.windSpeed = convertWindspeed(current.GetProperty("windspeed").GetDouble() / 3.6);

        p.windGust = current.GetProperty("windgust").TryGetDouble(out double dummy) == false ?
          (double)0.0f : convertWindspeed(dummy / 3.6);
        p.windBearing = degToBearing(p.windDirection);
        p.windUnit = ctx.Options.WindUnit;
        p.cloudCover = current.GetProperty("cloudcover").GetDouble();

        p.haveUVI = true;
        p.valid = true;
        p.apiId = "VC";
        // daily forecasts

        JsonElement day;
        for (int i = 1; i <= 3; i++) {
          day = root.GetProperty("days")[i];
          DateTime d = DateTime.Parse(day.GetProperty("datetime").ToString());
          daily.Add(new DailyForecast {
            date = d,
            code = getIcon(day.GetProperty("icon").ToString()),
            temperatureMin = day.GetProperty("tempmin").GetDouble(),
            temperatureMax = day.GetProperty("tempmax").GetDouble(),
            weekDay = d.ToString("ddd"),
            pop = day.GetProperty("precipprob").GetDouble()
          });
        }
      } catch (Exception e) when (e is FormatException or KeyNotFoundException) {
        p.valid = false;
        Log.Information(e.Message);
      }
    }

    /**
     * <summary>
     * This maps VC API condition descriptions to icon codes. These single-character codes
     * are needed to display the symbols from the weather font.
     * </summary>
     */
    private readonly Dictionary<string, char> icons = new Dictionary<string, char>
      {
        { "clear-day",            'a'},        {"clear-night",          'A'},
        { "cloudy",               'e'},        {"partly-cloudy-day",    'c'},
        { "partly-cloudy-night",  'C'},        {"fog",                  '0'},
        { "rain",                 'j'},        {"showers-day",          'g'},
        { "showers-night",        'g'},        {"snow",                 'o'},
        { "snow-showers-day",     'o'},        {"snow-showers-night",   'O'},
        { "thunder-showers-day",  'k'},        {"thunder-showers-night",'K'},
        { "thunder-rain",         'k'}
      };
  }
}