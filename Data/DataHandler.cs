/**
 * Copyright (c) 2012-2024 Alex Vie (as@subspace.cc)
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
using System.Text.Json;
using System.Text.Json.Nodes;
using System.Text.Json.Serialization;

using Spectre.Console;

namespace fetchweather.Data
{
  /**
   * helper class for the JsonSerializer to make it work with AOT.
   * it allows to serialize objects of type JsonDocument and JsonNode via source
   * generation
   */
  [JsonSourceGenerationOptions(GenerationMode = JsonSourceGenerationMode.Serialization)]
  [JsonSerializable(typeof(JsonDocument))]
  [JsonSerializable(typeof(JsonNode))]
  [JsonSerializable(typeof(DataPoint))]
  internal partial class MySerializerContext : JsonSerializerContext { }
  /**
   * the base class for all data handling implementations.
   */
  public partial class DataHandler
  {
    public DataHandler(string api)
    {
      this.apiProvider = api;
      ctx = GlobalContext.GetInstance();
      p.moonPhase = 0;
      p.moonPhaseAsString = "none";
      this.jsonOptions = new JsonSerializerOptions {
        WriteIndented = true,
        TypeInfoResolver = Data.MySerializerContext.Default
      };
    }

    protected string                finalUrl      { get; set; }
    protected string                finalUrlFC    { get; set; }
    protected virtual void          Populate()    {}
    protected virtual bool          Validate()    { return false; }
    public virtual Task<bool>       Fetch()       { return null; }

    internal  DataPoint             p = new DataPoint();
    internal  SolarData             solarData = new SolarData();
    /// <summary>daily forecasts</summary>
    internal  List<DailyForecast>   daily = new List<DailyForecast>();
    protected GlobalContext         ctx { get; set; }
    public    string                apiProvider { get; protected set; }
    public    string                apiKey { get; protected set; }
    public    JsonDocument          jsonDocument { get; protected set; }
    public    JsonDocument          jsonDocumentFC { get; protected set; }
    protected JsonSerializerOptions jsonOptions { get; set; }

    public partial void doOutput(TextWriter stream);
    public partial void doDump();
    protected partial Task<bool> readAndCacheSolarData();
    /**
     * NOTE: default arguments for partial methods must only be declared in
     * one place
     */
    public partial bool cacheResult<T>(T result, string provider = "NONE", string tag = "current", bool force = false);
    public partial JsonDocument readJsonFromCache(out bool result, string provider = "", string tag = "current");

    /**
     * <summary>
     * return the location (latitude,longitude) from either the command line
     * option or the option in the .INI file.
     * </summary>
     * <returns>the location as string in geo loc format (latitude,longitude)</returns>
     */
    protected string getLocation()
    {
      if (ctx.Options.location.Length > 0 && ctx.Options.location != "none") {
        return ctx.Options.location.Trim('"');
      }
      return ctx.iniData[apiProvider]["loc"].ToString();
    }

    /**
     * <summary>
     * convert pressure from hPa into inch hg (imperial). Source is always
     * in hPa nowadays.
     * </summary>
     * <param name="hPa">pressure (at sea level) in hPa</param>
     * <returns>pressure in inches hg</returns>
     */
    public double convertPressure(double hPa = 1013)
    {
      return ctx.Options.PressureUnit == "inhg" ? hPa / 33.863886666667 : hPa;
    }

    /**
     * <summary>Convert wind speed to various common units</summary>
     * <param name="speed">The wind speed in <b>meters per second</b> (m/s)</param>
     * <returns>The converted wind speed</returns>
     */
    public double convertWindspeed(double speed = 0)
    {
      string unit = ctx.Options.WindUnit;
      return unit switch {
        "km/h" => speed * 3.6,
        "mph" => speed * 2.237,
        "kts" => speed * 1.944,
        _ => speed,
      };
    }

    /**
     * <summary>
     * convert visibility from source unit to the user-selected display
     * unit. Some APIs provide visibility in meters, other in kilometers and maybe even
     * in miles. So we have to convert them.
     * </summary>
     * <param name="vis">The visibility source value</param>
     * <param name="source_unit">The unit for the source</param>
     * <returns>visibility in either km, miles or meters (user-selectable)</returns>
     */
    public double convertVis(double vis = 0, string source_unit = "m")
    {
      double source_vis = vis;
      switch (source_unit) {
        case "km":
          source_vis = vis * 1000;
          break;
        case "mi":
        case "miles":
          source_vis = vis * 1609;
          break;
      }

      return ctx.Options.VisUnit switch {
        "km" => source_vis / 1000,
        "mi" or "miles" => source_vis / 1609,
        // assuming source = in meters
        _ => source_vis,
      };
    }

    /**
     * <summary>
     * The 16 standard compass wind bearings
     * </summary>
     */
    private static readonly string[] wind_directions = {
      "N", "NNE", "NE", "ENE",
      "E", "ESE", "SE", "SSE",
      "S", "SSW", "SW", "WSW",
      "W", "WNW", "NW", "NNW"
    };
    /**
     * <summary>
     * Convert a numerical wind direction in degrees into a compass bearing.
     * For example: 90 degrees are east (E)
     * </summary>
     * <param name="wind_direction">The wind direction in degrees</param>
     * <returns>a string with the wind bearing</returns>
     */
    public static string degToBearing(int wind_direction = 0)
    {
      double wd = wind_direction switch {
        > 360 or < 0 => (double)0.0f,
        _ => (double)wind_direction,
      };
      int val = (int)((wd / 22.5) + 0.5);
      return DataHandler.wind_directions[val % 16];
    }

    /**
     * <summary>Convert a temperature. Input is assumed to be metric (celsius)</summary>
     * <param name="temp">The temperature in Celsius</param>
     * <returns>The converted temperature in Celsius or degrees Fahrenheit</returns>
     */
    public double convertTemperature(double temp)
    {
      return !ctx.Options.Metric ? (temp * (9.0 / 5.0)) + 32.0 : temp;
    }

    /**
     * <summary>
     * Output a temperature value. Convert it to imperial system if
     * necessary.
     * </summary>
     * <param name="stream">the output stream</param>
     * <param name="val">the temperature to print</param>
     * <param name="addUnit">add the unit when true</param>
     */
    public void outputTemperature(TextWriter stream, double val, bool addUnit = true)
    {
      string unit = ctx.Options.Metric ? "°C" : "°F";
      double res = this.convertTemperature(val);
      stream.Write(string.Format("{0:0.0}{1}\n", res, addUnit ? unit : ""));
    }

    /**
     * <summary>
     * Add the current DataPoint to the history database.
     * </summary>
     */
    public void AddHistory()
    {
      // respect the option and check whether our datapoint is considered valid
      if (ctx.Options.SkipDb == false && p.valid == true)
      {
        DB.Database db = new DB.Database();
        db.AddRecord(p);
        db.Dispose();
      }
    }

    public void DumpDebugInfo()
    {
      AnsiConsole.MarkupLine("[yellow bold]API Provider Info [red](Caution: this contains your API key. Do not share)[/]:[/]");
      AnsiConsole.MarkupLine("[green]Provider: [/]{0}", this.apiProvider);
      AnsiConsole.MarkupLine("[green]Key:      [/]{0}", this.apiKey);
      AnsiConsole.MarkupLine("[green]Baseurl:  [/]{0}", this.finalUrl);
      if (this.finalUrlFC != "") {
        AnsiConsole.MarkupLine("[green]FCUrl:    [/]{0}", this.finalUrlFC);
      }
    }
  }
}