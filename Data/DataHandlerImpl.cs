/**
 * Copyright (c) 2012-2024 Alex Vie (as@subspace.cc)
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
using System.Text.Json;
using System.Text.Json.Nodes;
using Serilog.Context;
using Serilog;
using System.Xml.Serialization;

namespace fetchweather.Data
{
  public partial class DataHandler
  {
    public partial void doOutput(TextWriter stream)
    {
      LogContext.PushProperty("ctx", "DataHandler.doOutput()");
      Log.Information("Begin output to {0}", stream.ToString());
      stream.Write("** Begin output **\n");
      stream.WriteLine(p.weatherSymbol);
      outputTemperature(stream, p.temperature, true);

      for (int i = 0; i < 3; i++) {
        stream.WriteLine(daily[i].code);
        outputTemperature(stream, daily[i].temperatureMin, addUnit: false);
        outputTemperature(stream, daily[i].temperatureMax, addUnit: false);
        stream.WriteLine(daily[i].weekDay);
      }

      outputTemperature(stream, p.temperatureApparent);
      outputTemperature(stream, p.dewPoint);

      stream.WriteLine(string.Format("Humidity: {0:.#}", p.humidity));
      stream.WriteLine(string.Format(ctx.Options.PressureUnit == "hPa" ? "{0:.#} hPa" : "{0:.##} InHg", p.pressureSeaLevel));

      stream.WriteLine(string.Format("{0:0.0} {1}", p.windSpeed, ctx.Options.WindUnit));

      if (p.precipitationIntensity > 0) {
        stream.Write(string.Format("{0} ({1:0.0}mm/1h)\n", p.precipitationTypeAsString, p.precipitationIntensity));
      } else {
        stream.Write(string.Format("PoP: {0:0.0}%\n", p.precipitationProbability));
      }

      stream.Write(string.Format("{0:0.0} {1}\n", p.visibility, ctx.Options.VisUnit));
      stream.WriteLine(p.sunriseTimeAsString);
      stream.WriteLine(p.sunsetTimeAsString);
      stream.WriteLine(p.windBearing);
      stream.WriteLine(p.timeRecordedAsText);

      stream.Write(p.conditionAsString);
      if (p.cloudCover > 0) {
        stream.Write(string.Format(" ({0:0.0}%)\n", p.cloudCover));
      } else {
        stream.Write("\n");
      }

      stream.WriteLine(p.timeZone);
      outputTemperature(stream, p.temperatureMin, addUnit: true);
      outputTemperature(stream, p.temperatureMax, addUnit: true);

      if (p.haveUVI) {
        stream.Write(string.Format("UV: {0:0.0}\n", this.p.uvIndex));
      } else {
        stream.Write("0\n");
      }

      stream.WriteLine("** end data **");
      stream.WriteLine(string.Format("{0:0}% (Clouds)", p.cloudCover));
      stream.WriteLine(string.Format("{0:0} (Cloudbase)", p.cloudBase));
      stream.WriteLine(string.Format("{0:0} (Cloudceil)", p.cloudCeiling));
      stream.WriteLine(string.Format("{0} (Moon)", p.moonPhase));
      stream.WriteLine(p.apiId);
    }

    /**
     * <summary>
     * Dump the current datapoint to a file. Supports json, plaintext and XML
     * </summary>
     */
    public partial void doDump()
    {
      if (ctx.Options.dumpTo != "" && p.valid) {
        LogContext.PushProperty("ctx", "DataHandler.doDump()" + ctx.Options.dumpTo);
        switch(ctx.Options.DumpFormat) {
          case "json":
            Log.Information("Dumping in JSON format to {0}", ctx.Options.dumpTo);
            try {
              StreamWriter w = new StreamWriter(ctx.Options.dumpTo);
              string json = JsonSerializer.Serialize<DataPoint>(this.p, this.jsonOptions);
              w.Write(JsonSerializer.Serialize(this.p, this.jsonOptions));
              w.Dispose();
            } catch (Exception e) when (e is NotSupportedException or IOException) {
              Log.Error("Error writing JSON dump to file {0}", ctx.Options.dumpTo);
              Log.Error(e.Message);
            }
            break;
          case "plain":
            Log.Information("Dumping in plain text format to {0}", ctx.Options.dumpTo);
            try {
              using StreamWriter writer = File.CreateText(ctx.Options.dumpTo);
              doOutput(writer);
            } catch (Exception e) {
              Log.Error("Error writing text dump to file {0}", ctx.Options.dumpTo);
              Log.Error(e.Message);
            }
            break;
          case "xml":
            Log.Information("Dumping in XML format to {0}", ctx.Options.dumpTo);
            XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(typeof(DataPoint));
            try {
              using StreamWriter writer = File.CreateText(ctx.Options.dumpTo);
              serializer.Serialize(writer, this.p);
            } catch (Exception e) {
              Log.Error("Error writing XML dump to file {0}", ctx.Options.dumpTo);
              Log.Error(e.Message);
            }
            break;
          default:
            Log.Information("No valid dump format found");
            break;
        }
      }
    }
    /**
     * <summary>
     * fetch the solar data (sunrise, sunset) from api.sunrisesunset.io and
     * cache it. We only need this once per day, obviously.
     * </summary>
     * <returns>true on success</returns>
     */
    protected async partial Task<bool> readAndCacheSolarData()
    {
      DateTime now = DateTime.Now;
      bool needRefresh = false;
      solarData.valid = false;

      string location = getLocation();
      string[] latlon = location.Trim('"').Split(',', 2);
      LogContext.PushProperty("ctx", "DataHandler.readAndCacheSolarData()");
      JsonDocument cached = readJsonFromCache(out bool result, tag: "solar");
      if (cached == null) {
        Log.Information("no cache found for API {0}", apiProvider);
        needRefresh = true;
      } else {
        DateTime dt = DateTime.Parse(cached.RootElement.GetProperty("results").GetProperty("date").ToString());
        Log.Information("Cached solar data found for {0}", dt);
        solarData.sunriseTime = DateTime.Parse(cached.RootElement.GetProperty("results").GetProperty("sunrise").ToString());
        solarData.sunsetTime = DateTime.Parse(cached.RootElement.GetProperty("results").GetProperty("sunset").ToString());
        if (cached.RootElement.TryGetProperty("location", out JsonElement loc)) {
          solarData.location = loc.ToString();
        } else {
          solarData.location = location;
        }
        solarData.valid = true;
        if (now.Date != dt.Date) {      // too old, refresh it. once per day is enough
          needRefresh = true;
        }
        if (solarData.location != location) {
          needRefresh = true;
        }
      }
      if (needRefresh && !ctx.Options.Offline) {
        try {
          if (latlon.Length != 2) {
            Log.Error("Latitude / Longitude wrong format. Must be lat,lon");
            return false;
          }
          string url = string.Format("https://api.sunrisesunset.io/json?lat={0}&lng={1}", latlon[0], latlon[1]);
          Log.Information("attempt to fetch solar data from {0}", url);
          using HttpResponseMessage r = await ctx.httpClient.GetAsync(url);
          r.EnsureSuccessStatusCode();
          string jsonResult = await r.Content.ReadAsStringAsync();
          JsonNode json = JsonNode.Parse(jsonResult);
          if (json != null) {
            json["location"] = location;
            json["last_updated"] = now.ToString();
            cacheResult(json, tag: "solar");
            string sunrise = json["results"]?["sunrise"]?.GetValue<string>();
            string sunset = json["results"]?["sunset"]?.GetValue<string>();
            if (sunrise != null && sunset != null) {
              solarData.sunriseTime = DateTime.Parse(sunrise);
              solarData.sunsetTime = DateTime.Parse(sunset);
            } else {
              solarData.sunriseTime = now;
              solarData.sunsetTime = now;
            }
            solarData.location = location;
            solarData.valid = true;
          }
          Log.Information("solar data sucessfully refreshed");
          ctx.CallStats.updateStats("SolarData", amount: 1, bytes: jsonResult.Length);
          return true;
        } catch (Exception e) when (e is HttpRequestException or InvalidOperationException or UriFormatException) {
          Log.Error("error fetching solar data");
          Log.Error(e.Message);
          solarData.valid = false;
          return false;
        }
      }
      return false;
    }

    /**
     * <summary>
     * read a json document from a cache file, parse it and return a
     * JsonDocument</summary>
     * <param name="result">bool true on success</param>
     * <param name="provider">the API provider shortcut</param>
     * <param name="tag">the filename tag</param>
     * <returns>JsonDocument object</returns>
     */
    public partial JsonDocument readJsonFromCache(out bool result, string provider, string tag)
    {
      if (provider == "") {
        provider = this.apiProvider;
      }
      string filename = provider + "_" + tag + ".json";
      try {
        using StreamReader r = new StreamReader(Path.Combine(ctx.cacheDir, filename));
        string jsonResult = r.ReadToEnd();
        r.Close();
        JsonDocument json = JsonDocument.Parse(jsonResult);
        result = true;
        return json;
      } catch (Exception e) when (e is IOException or JsonException or ArgumentException) {
        Log.Error("Error reading cache file {0}", filename);
        Log.Error(e.Message);
        result = false;
        return null;
      }
    }

    /**
     * <summary>
     * write a JsonDocument to a cache file. The filename is built from the
     * api provider shortcut and a tag.
     * </summary>
     * <param name="result">The JsonDocument to write</param>
     * <param name="provider">The API provider shortcut</param>
     * <param name="tag">A tag from which the base filename is built</param>
     * <returns>true on succes</returns>
     */
    public partial bool cacheResult<T>(T result, string provider, string tag, bool force)
    {
      if (ctx.Options.SkipCache && force == false) {
        return true;
      }
      if (provider == "NONE") {
        provider = this.apiProvider;
      }
      string filename = provider + "_" + tag + ".json";
      try {
        using StreamWriter w = new StreamWriter(Path.Combine(ctx.cacheDir, filename));
        w.Write(JsonSerializer.Serialize(result, this.jsonOptions));
        w.Close();
      } catch (Exception e) when (e is IOException or JsonException or ArgumentException) {
        Log.Error("Error writing cache file {0}", filename);
        Log.Error(e.Message);
        return false;
      }
      return true;
    }
  }
}