/**
 * Copyright (c) 2012-2024 Alex Vie (as@subspace.cc)
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
//#pragma warning disable CA1051

using Serilog;
using Serilog.Context;

namespace fetchweather.Data
{
  public record DataPoint
  {
    public string       Api                        { get; set; }
    public bool         is_day                     { get; set; }
    public bool         valid                      { get; set; }
    public DateTime     timeRecorded               { get; set; }
    public DateTime     sunsetTime                 { get; set; }
    public DateTime     sunriseTime                { get; set; }
    public string       timeRecordedAsText         { get; set; }
    public string       timeZone                   { get; set; }
    public int          weatherCode                { get; set; }
    public char         weatherSymbol              { get; set; }
    public double       temperature                { get; set; }
    public double       temperatureApparent        { get; set; }
    public double       temperatureMin             { get; set; }
    public double       temperatureMax             { get; set; }
    public double       visibility                 { get; set; }
    public double       windSpeed                  { get; set; }
    public double       windGust                   { get; set; }
    public double       cloudCover                 { get; set; }
    public double       cloudBase                  { get; set; }
    public double       cloudCeiling               { get; set; }
    public int          moonPhase                  { get; set; }
    public string       moonPhaseAsString          { get; set; }
    public int          windDirection              { get; set; }
    public int          precipitationType          { get; set; }
    public string       precipitationTypeAsString  { get; set; }
    public double       precipitationProbability   { get; set; }
    public double       precipitationIntensity     { get; set; }
    public double       pressureSeaLevel           { get; set; }
    public double       humidity                   { get; set; }
    public double       dewPoint                   { get; set; }
    public string       sunsetTimeAsString         { get; set; }
    public string       sunriseTimeAsString        { get; set; }
    public string       windBearing                { get; set; }
    public string       windUnit                   { get; set; }
    public string       conditionAsString          { get; set; }
    public double       uvIndex                    { get; set; }
    public bool         haveUVI                    { get; set; }
    public string       apiId                      { get; set; }

    /**
     * <summary>Convert a temperature. Input is assumed to be metric (celsius)</summary>
     * <param name="temp">The temperature in Celsius</param>
     * <returns>The converted temperature in Celsius or degrees Fahrenheit</returns>
     */
    private double convertTemperature(double temp)
    {
      return !ctx.Options.Metric ? (temp * (9.0 / 5.0)) + 32.0 : temp;
    }

    private void outputTemperature(TextWriter stream, double val, bool addUnit = true)
    {
      string unit = ctx.Options.Metric ? "°C" : "°F";
      double res = this.convertTemperature(val);
      stream.Write(string.Format("{0:0.0}{1}\n", res, addUnit ? unit : ""));
    }

    public void DoOutput(TextWriter stream)
    {
      this.ctx = GlobalContext.GetInstance();
      LogContext.PushProperty("ctx", "DataPoint.DoOutput()");
      Log.Information("Begin output to {0}", stream.ToString());
    }

    private GlobalContext ctx;
  }

  internal record DailyForecast
  {
    public DateTime     date;
    public char         code;
    public double       temperatureMin, temperatureMax;
    public string       weekDay;
    public double       pop;
  }

  internal record SolarData
  {
    public DateTime     sunriseTime, sunsetTime;
    public string       location;
    public bool         valid;
  }
}