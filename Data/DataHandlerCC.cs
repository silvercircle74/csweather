/**
 * Copyright (c) 2012-2024 Alex Vie (as@subspace.cc)
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
using Serilog;
using Serilog.Context;
using System.Text.Json;
using System.Text;
using Tommy;

namespace fetchweather.Data
{
  /**
   * This implements the Tomorrow.IO API. For historical reasons, it's called CC
   * for ClimaCell, the previous name of tomorrow.io
   * CC is a multicall API. It needs separate calls for current conditions
   * and forecast.
   */
  public class DataHandlerCC : DataHandler
  {
    public DataHandlerCC() : base("CC")
    {
      TomlTable id = ctx.iniData;
      // api key may be overridden with --apikey command line option.
      apiKey = ctx.Options.ApiKey != "nokey" ? ctx.Options.ApiKey : id[apiProvider]["apikey"];
      string baseurl = id[apiProvider]["baseurl"].ToString();
      StringBuilder builder = new StringBuilder(baseurl).Append(apiKey);
      builder.Append("&location=").Append(getLocation());
      builder.Append("&timezone=").Append(id[apiProvider]["timezone"].ToString())
        .Append("&fields=weatherCode,temperature,temperatureApparent,visibility,windSpeed,windDirection,")
        .Append("precipitationType,precipitationProbability,pressureSeaLevel,windGust,cloudCover,cloudBase,")
        .Append("cloudCeiling,humidity,precipitationIntensity,dewPoint,")
        .Append("rainIntensity,snowIntensity,uvIndex&timesteps=current&units=metric");
      finalUrl = builder.ToString();

      // the URL for the forecast is similar, so we can re-use the base url.
      builder.Clear();
      builder.Append(baseurl).Append(apiKey);
      builder.Append("&location=").Append(id[apiProvider]["loc"].ToString());
      builder.Append("&timezone=").Append(id[apiProvider]["timezone"].ToString())
        .Append("&fields=weatherCode,temperatureMax,temperatureMin,sunriseTime,sunsetTime,moonPhase,")
        .Append("precipitationType,precipitationProbability&timesteps=1d&startTime=");

      finalUrlFC = builder.ToString();
    }

    /**
     * <summary>
     * This performs all the work. It fetches the JSON documents from the API and
     * parses them into a JsonDocument object.
     *
     * in offline mode, it retrieves the json from a cached (if available). The cache
     * is updated in online mode.
     * </summary>
     * <returns>returns true on success.</returns>
     */
    public override async Task<bool> Fetch()
    {
      bool rc = await readAndCacheSolarData();
      bool tryCache = false;
      LogContext.PushProperty("ctx", "DataHandlerCC.Fetch()");

      bool TryCache()
      {
        Log.Logger.Information("Offline - trying cached data");
        this.jsonDocument = readJsonFromCache(out bool result, tag: "current");
        if (this.jsonDocument != null && result == true) {
          this.jsonDocumentFC = readJsonFromCache(out result, tag: "forecast");
          if (this.jsonDocumentFC != null && result == true) {
            if (Validate() == true) {
              Populate();
              return true;
            }
          }
        }
        return false;
      }

      if (ctx.Options.Offline == true) {
        return TryCache();
      }
      try {
        Log.Logger.Information("Fetching current conditions from ClimaCell API");
        using HttpResponseMessage r = await ctx.httpClient.GetAsync(finalUrl);
        r.EnsureSuccessStatusCode();
        string jsonResult = await r.Content.ReadAsStringAsync();
        long length = jsonResult.Length;
        jsonDocument = JsonDocument.Parse(jsonResult);

        Log.Logger.Information("Fetching forecast from ClimaCell API");
        using HttpResponseMessage r1 = await ctx.httpClient.GetAsync(finalUrlFC);
        r1.EnsureSuccessStatusCode();
        jsonResult = await r1.Content.ReadAsStringAsync();
        length += jsonResult.Length;
        jsonDocumentFC = JsonDocument.Parse(jsonResult);

        Log.Information("Fetch from CC API successful");
        ctx.CallStats.updateStats(apiProvider, amount: 2, bytes: length);
        if (Validate() == true) {
          Populate();
          AddHistory();
          if (p.valid == true) {
            cacheResult(jsonDocument, tag: "current");
            cacheResult(jsonDocumentFC, tag: "forecast");
          }
          return true;
        }
        tryCache = ctx.Options.TryCache;
      } catch (Exception e) when (e is HttpRequestException or JsonException) {
        Log.Warning(e.Message);
        tryCache = ctx.Options.TryCache;
      }

      if (tryCache)
      {
        return TryCache();
      }
      return false;
    }

    /**
     * <summary>
     * validate the json document. This runs a few tests and checks whether
     * the JSON contains valid data.
     * </summary>
     * <returns>true on success</returns>
     */
    protected override bool Validate()
    {
      LogContext.PushProperty("ctx", "DataHandlerCC.Validate()");
      try {
        bool rc = true;
        Log.Information("Validating current conditions");
        JsonElement current = jsonDocument.RootElement.GetProperty("data").GetProperty("timelines")[0].GetProperty("intervals")[0];
        JsonElement values = current.GetProperty("values");

        rc &= values.GetProperty("temperature").TryGetDouble(out double dummy);
        rc &= values.GetProperty("windDirection").TryGetDouble(out dummy);
        rc &= values.GetProperty("cloudCover").TryGetDouble(out dummy);

        // verify forecast
        JsonElement days = jsonDocumentFC.RootElement.GetProperty("data").GetProperty("timelines")[0].GetProperty("intervals");

        Log.Information("Validating forecast (3 days)");
        for (int i = 0; i < 3; i++) {
          rc &= days[i].GetProperty("values").GetProperty("temperatureMin").TryGetDouble(out dummy);
          rc &= days[i].GetProperty("values").GetProperty("weatherCode").TryGetInt32(out int intdummy);
        }
        Log.Information("Validate: success");
        return rc;
      } catch (Exception e) {
        Log.Warning(e.Message);
      }
      return false;
    }

    /**
     * <summary>Populate the DataPoint record. It must set p.valid to
     * true on success
     * </summary>
     */
    protected override void Populate()
    {
      LogContext.PushProperty("ctx", "DataHandlerCC.Populate()");

      Log.Information("Processing current conditions");
      JsonElement current = jsonDocument.RootElement.GetProperty("data").GetProperty("timelines")[0].GetProperty("intervals")[0];
      JsonElement day = jsonDocumentFC.RootElement.GetProperty("data").GetProperty("timelines")[0].GetProperty("intervals")[0];
      JsonElement values = current.GetProperty("values");

      p.timeRecorded = DateTime.Parse(current.GetProperty("startTime").ToString());
      p.timeRecordedAsText = p.timeRecorded.ToString("HH:mm");
      p.weatherCode = values.GetProperty("weatherCode").GetInt32();
      if (solarData.valid) {
        p.sunriseTime = solarData.sunriseTime;
        p.sunsetTime = solarData.sunsetTime;
      } else {
        p.sunriseTime = DateTime.Parse(day.GetProperty("values").GetProperty("sunriseTime").ToString());
        p.sunsetTime = DateTime.Parse(day.GetProperty("values").GetProperty("sunsetTime").ToString());
      }
      p.sunriseTimeAsString = p.sunriseTime.ToString("HH:mm");
      p.sunsetTimeAsString = p.sunsetTime.ToString("HH:mm");
      p.is_day = p.timeRecorded >= p.sunriseTime && p.timeRecorded <= p.sunsetTime;
      p.weatherSymbol = getIcon(p.weatherCode, p.is_day);
      p.timeZone = ctx.iniData[apiProvider]["timezone"];
      p.conditionAsString = getCondition(p.weatherCode);

      p.temperature = values.GetProperty("temperature").GetDouble();
      p.temperatureApparent = values.GetProperty("temperatureApparent").GetDouble();
      p.dewPoint = values.GetProperty("dewPoint").GetDouble();
      p.temperatureMin = day.GetProperty("values").GetProperty("temperatureMin").GetDouble();
      p.temperatureMax = day.GetProperty("values").GetProperty("temperatureMax").GetDouble();

      p.pressureSeaLevel = convertPressure(values.GetProperty("pressureSeaLevel").GetDouble());
      p.visibility = convertVis(values.GetProperty("visibility").GetDouble(), source_unit: "km");
      p.humidity = values.GetProperty("humidity").GetDouble();

      p.windDirection = (int)values.GetProperty("windDirection").GetDouble();
      p.windGust = convertWindspeed(values.GetProperty("windGust").GetDouble());
      p.windSpeed = convertWindspeed(values.GetProperty("windSpeed").GetDouble());
      p.windUnit = ctx.Options.WindUnit;
      p.windBearing = degToBearing(p.windDirection);

      try {
        p.cloudBase = values.GetProperty("cloudBase").TryGetDouble(out double idummy) ? idummy : 0.0;
        p.cloudCeiling = values.GetProperty("cloudCeiling").TryGetDouble(out idummy) ? idummy : 0.0;
      } catch (Exception e) when (e is InvalidOperationException) {
        p.cloudBase = 0;
        p.cloudCeiling = 0;
      }
      p.cloudCover = values.GetProperty("cloudCover").TryGetDouble(out double dummy) ? dummy : 0.0;

      p.precipitationProbability = values.GetProperty("precipitationProbability").TryGetDouble(out dummy) ? dummy : 0.0;
      p.precipitationIntensity = values.GetProperty("precipitationIntensity").GetDouble();
      if (p.precipitationIntensity > 0) {
        p.precipitationType = values.GetProperty("precipitationType").GetInt32();
        p.precipitationTypeAsString = (p.precipitationType is >= 0 and <= 4) ?
          precipTypes[p.precipitationType] : "";
      } else {
        p.precipitationTypeAsString = "";
        p.precipitationType = 0;
      }
      p.apiId = "CC";
      p.Api = "ClimaCell";
      p.haveUVI = true;
      p.uvIndex = values.GetProperty("uvIndex").GetDouble();

      Log.Information("Processing daily forecast");
      JsonElement days = jsonDocumentFC.RootElement.GetProperty("data").GetProperty("timelines")[0].GetProperty("intervals");
      for (int i = 1; i <= 3; i++) {
        JsonElement this_day = days[i].GetProperty("values");
        DateTime d = DateTime.Parse(days[i].GetProperty("startTime").ToString());
        daily.Add(new DailyForecast {
          date = d,
          code = getIcon(this_day.GetProperty("weatherCode").GetInt32(), p.is_day),
          temperatureMax = this_day.GetProperty("temperatureMax").GetDouble(),
          temperatureMin = this_day.GetProperty("temperatureMin").GetDouble(),
          weekDay = d.ToString("ddd"),
          pop = this_day.GetProperty("precipitationProbability").GetDouble()
        });
      }
      Log.Information("Populating data point completed");
      p.valid = true;
    }

    /**
     * <summary>
     * get the icon code for using with the weather font.
     * </summary>
     * <param name="code">an integer code, describing the current condition</param>
     * <param name="isDay">use day or night icon</param>
     * <returns>the icon code</returns>
     */
    protected char getIcon(int code = 1000, bool isDay = true)
    {
      icons.TryGetValue(code, out string value);
      return value switch {
        null => isDay ? 'a' : 'A',
        _ => value[isDay ? 0 : 1]
      };
    }

    /**
     * <summary>map weather codes to readable conditions</summary>
     * <param>code</param>
     */
    protected string getCondition(int code = 1000)
    {
      conditions.TryGetValue(code, out string value);
      return value ?? conditions[1000];
    }

    private readonly string[] precipTypes =  { "", "Rain", "Snow", "Freezing Rain", "Ice Pellets" };

    private readonly Dictionary<int, string> icons = new Dictionary<int, string>
    {
      {1000, "aA"}, {1001, "ef"},
      {1100, "bB"}, {1101, "cC"},
      {1102, "dD"}, {2000, "00"},
      {2100, "77"}, {3000, "99"},
      {3001, "99"}, {3002, "23"},
      {4000, "xx"}, {4001, "gG"},
      {4200, "gg"}, {4201, "jj"},
      {5000, "oO"}, {5001, "xx"},
      {5100, "oO"}, {5101, "ww"},
      {6000, "xx"}, {6001, "yy"},
      {6200, "ss"}, {6201, "yy"},
      {7000, "uu"}, {7001, "uu"},
      {7102, "uu"}, {8000, "kK"}
    };
    private readonly Dictionary<int, string> conditions = new Dictionary<int, string>
    {
       {1000, "Clear"},               {1001, "Cloudy"},
       {1100, "Mostly Clear"},        {1101, "Partly Cloudy"},
       {1102, "Mostly Cloudy"},       {2000, "Fog"},
       {2100, "Light Fog"},           {3000, "Light Wind"},
       {3001, "Wind"},                {3002, "Strong Wind"},
       {4000, "Drizzle"},             {4001, "Rain"},
       {4200, "Light Rain"},          {4201, "Heavy Rain"},
       {5000, "Snow"},                {5001, "Flurries"},
       {5100, "Light Snow"},          {5101, "Heavy Snow"},
       {6000, "Freezing Drizzle"},    {6001, "Freezing Rain"},
       {6200, "Light Freezing Rain"}, {6201, "Heavy Freezing Rain"},
       {7000, "Ice Pellets"},         {7001, "Heavy Ice Pellets"},
       {7102, "Light Ice Pellets"},   {8000, "Thunderstorm"}
    };
  }
}