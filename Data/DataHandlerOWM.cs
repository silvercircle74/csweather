/**
 * Copyright (c) 2012-2024 Alex Vie (as@subspace.cc)
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
using Serilog;
using Serilog.Context;
using System.Text.Json;
using System.Text;
using fetchweather.Utils;
using Tommy;
namespace fetchweather.Data
{
  /**
   * This implements the OpenWeatherMap API. OWM uses a singlecall API.
   */
  public class DataHandlerOWM : DataHandler
  {
    public DataHandlerOWM() : base("OWM")
    {
      LogContext.PushProperty("ctx", "DataHandlerOWM.Construct");
      TomlTable id = ctx.iniData;
      // api key may be overridden with --apikey command line option.
      apiKey = ctx.Options.ApiKey != "nokey" ? ctx.Options.ApiKey : id[apiProvider]["apikey"];
      string baseurl = id[apiProvider]["baseurl"].ToString();
      StringBuilder builder = new StringBuilder(baseurl).Append(apiKey);
      string location = getLocation();
      string[] locs = location.Split(',', 2);
      if (locs.Length == 2) {
        builder.Append("&lat=").Append(locs[0]);
        builder.Append("&lon=").Append(locs[1]);
      } else {
        Log.Warning("Location format probably invalid");
      }
      builder.Append("&exclude=minutely&units=metric");

      finalUrl = builder.ToString();
    }

    /**
     * <summary>
     * This performs all the work. It fetches the JSON documents from the API and
     * parses them into a JsonDocument object.
     *
     * in offline mode, it retrieves the json from a cached version (if available). The cache
     * is updated in online mode.
     * </summary>
     * <returns>returns true on success.</returns>
     */
    public override async Task<bool> Fetch()
    {
      bool rc = await readAndCacheSolarData();
      bool tryCache = false;
      LogContext.PushProperty("ctx", "DataHandlerOWM.Fetch()");

      bool TryCache()
      {
        this.jsonDocument = readJsonFromCache(out bool result, tag: "current");
        if (this.jsonDocument != null && result == true) {
          Populate();
          return true;
        }
        return false;
      }

      if (ctx.Options.Offline) {
        Log.Information("Offline mode. Trying Cache.");
        return TryCache();
      }

      try {
        Log.Information("Try to fetch from api at {0}", finalUrl);
        using HttpResponseMessage r = await ctx.httpClient.GetAsync(finalUrl);
        r.EnsureSuccessStatusCode();
        string jsonResult = await r.Content.ReadAsStringAsync();
        jsonDocument = JsonDocument.Parse(jsonResult);
        ctx.CallStats.updateStats(apiProvider, amount: 1, bytes: jsonResult.Length);
        if (Validate() == true) {
          Populate();
          AddHistory();
          if (p.valid == true) {
            cacheResult(jsonDocument, tag: "current");
          }
          return true;
        }
        tryCache = ctx.Options.TryCache;
      } catch (Exception e) when (e is HttpRequestException or JsonException) {
        Log.Error("Exception when fetching from API");
        Log.Error(e.Message);
        tryCache = ctx.Options.TryCache;
      }
      if (tryCache) {
        Log.Warning("Fetching from API failed, trying cache (--try-cache option)");
        return TryCache();
      }
      return false;
    }

    protected override void Populate()
    {
      JsonElement current = jsonDocument.RootElement.GetProperty("current");
      JsonElement days = jsonDocument.RootElement.GetProperty("daily");
      JsonElement hour = jsonDocument.RootElement.GetProperty("hourly")[0];

      LogContext.PushProperty("ctx", "DataHandlerOWM.Populate()");
      p.timeZone = jsonDocument.RootElement.GetProperty("timezone").ToString();
      p.Api = "Open Weather Map";
      p.apiId = "OWM";

      long tzOffset = jsonDocument.RootElement.GetProperty("timezone_offset").GetInt32();
      p.timeZone = jsonDocument.RootElement.GetProperty("timezone").ToString();
      p.timeRecorded = Util.FromUnixEpoch(current.GetProperty("dt").GetInt32() + tzOffset);
      p.timeRecordedAsText = p.timeRecorded.ToString("HH:mm");
      Log.Information("Creating Dataset for {0}", p.timeRecordedAsText);

      if (solarData.valid) {
        Log.Information("Found valid solar data");
        p.sunriseTime = solarData.sunriseTime;
        p.sunsetTime = solarData.sunsetTime;
      } else {
        Log.Warning("Solar data possibly invalid. Setting default values from weather API. Might be inaccurate");
        p.sunriseTime = Util.FromUnixEpoch(current.GetProperty("sunrise").GetInt32());
        p.sunsetTime =  Util.FromUnixEpoch(current.GetProperty("sunset").GetInt32());
      }
      p.is_day = p.timeRecorded >= p.sunriseTime && p.timeRecorded <= p.sunsetTime;
      p.sunsetTimeAsString = p.sunsetTime.ToString("HH:mm");
      p.sunriseTimeAsString = p.sunriseTime.ToString("HH:mm");
      p.temperatureMax = days[0].GetProperty("temp").GetProperty("max").GetDouble();
      p.temperatureMin = days[0].GetProperty("temp").GetProperty("min").GetDouble();

      p.temperature = current.GetProperty("temp").GetDouble();
      p.temperatureApparent = current.GetProperty("feels_like").GetDouble();
      p.dewPoint = current.GetProperty("dew_point").GetDouble();

      p.weatherCode = current.GetProperty("weather")[0].GetProperty("id").GetInt32();
      p.weatherSymbol = getIcon(p.weatherCode);

      p.visibility = convertVis(current.GetProperty("visibility").GetInt32(), source_unit: "m");
      p.humidity = current.GetProperty("humidity").GetDouble();
      p.pressureSeaLevel = convertPressure(current.GetProperty("pressure").GetDouble());
      p.windGust = 0;
      p.windSpeed = convertWindspeed(current.GetProperty("wind_speed").GetDouble());
      p.windDirection = current.GetProperty("wind_deg").GetInt32();
      p.windBearing = degToBearing(p.windDirection);
      p.windUnit = ctx.Options.WindUnit;
      p.conditionAsString = current.GetProperty("weather")[0].GetProperty("main").ToString();
      p.cloudCover = current.GetProperty("clouds").GetDouble();

      p.precipitationProbability = hour.GetProperty("pop").GetDouble();
      p.precipitationType = 0;
      p.precipitationIntensity = 0;
      p.precipitationTypeAsString = "";

      if (current.TryGetProperty("rain", out JsonElement jrain) == true) {
        p.precipitationIntensity = jrain.GetProperty("1h").GetDouble();
        p.precipitationType = 1;
        p.precipitationTypeAsString = "Rain";
      }
      if (current.TryGetProperty("snow", out JsonElement jsnow) == true) {
        p.precipitationIntensity = jsnow.GetProperty("1h").GetDouble();
        p.precipitationType = 2;
        p.precipitationTypeAsString = "Snow";
      }

      // daily forecasts
      for (int i = 1; i <= 3; i++) {
        JsonElement this_day = days[i];
        DateTime d = Util.FromUnixEpoch(this_day.GetProperty("dt").GetInt32() + tzOffset);
        daily.Add(new DailyForecast {
          date = d,
          code = getIcon(this_day.GetProperty("weather")[0].GetProperty("id").GetInt32()),
          temperatureMax = this_day.GetProperty("temp").GetProperty("max").GetDouble(),
          temperatureMin = this_day.GetProperty("temp").GetProperty("min").GetDouble(),
          weekDay = d.ToString("ddd"),
          pop = this_day.GetProperty("pop").GetDouble()
        });
      }
      p.haveUVI = true;
      p.uvIndex = current.GetProperty("uvi").GetDouble();
      p.valid = true;
      Log.Information("Populate complete. Dataset valid.");
    }

    /**
     * <summary>
     * retrieve the weather icon for the API-specific condition code
     * </summary>
     * <param name="code">the condition code as received from the API</param>
     * <returns>a weather icon character code</returns>
     */
    protected char getIcon(int code)
    {
      bool is_day = p.is_day;
      char symbol = 'c';

      if (code is >= 200 and <= 299) {         // thunderstorm
        symbol = is_day ? 'k' : 'K';
      } else if (code is >= 300 and <= 399) {
        symbol = 'x';
      } else if (code == 800) {
        symbol = is_day ? 'a' : 'A';
      } else if (code is >= 500 and <= 599) {    // rain
        symbol = code switch {
          511 => 's',
          502 or 503 or 504 => 'j',
          _ => is_day ? 'g' : 'G'
        };
      } else if (code is >= 600 and <= 699) {     // snow
        symbol = code switch {
          602 or 522 => 'w',
          504 => 'j',
          _ => is_day ? 'o' : 'O'
        };
      } else if (code is >= 801 and <= 899) {     // other
        symbol = code switch {
          801 => is_day ? 'b' : 'B',
          802 => is_day ? 'c' : 'C',
          803 => is_day ? 'e' : 'f',
          804 => is_day ? 'd' : 'D',
          _ => is_day ? 'c' : 'C'
        };
      } else if (code is >= 700 and <= 799) {
        symbol = code switch {
          711 or 741 or 701 => '0',
          _ => is_day ? 'c' : 'C'
        };
      }
      return symbol;
    }

    /**
     * <summary>Validate the JSON data</summary>
     * <returns>validation status. false if errors were found</returns>
     */
    protected override bool Validate()
    {
      bool success = true;
      LogContext.PushProperty("ctx", "DataHandlerOWM.Validate()");
      try {
        Log.Information("Validating JSON data");
        JsonElement current = jsonDocument.RootElement.GetProperty("current");
        JsonElement days = jsonDocument.RootElement.GetProperty("daily");
        JsonElement hour = jsonDocument.RootElement.GetProperty("hourly")[0];
        success &= current.GetProperty("weather")[0].GetProperty("id").TryGetInt32(out int iDummy);
        success &= current.GetProperty("dt").TryGetInt32(out iDummy);
      } catch (Exception e) {
        Log.Error("Error in validation");
        Log.Error(e.Message);
        success = false;
      } finally {
        Log.Information("Validation complete");
      }
      return success;
    }
  }
}