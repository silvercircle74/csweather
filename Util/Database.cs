/**
 * Copyright (c) 2012-2024 Alex Vie (as@subspace.cc)
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
using Microsoft.Data.Sqlite;
using Serilog;
using fetchweather.Utils;

namespace fetchweather.DB
{
  public class Database : IDisposable
  {
    public Database()
    {
      GlobalContext ctx = GlobalContext.GetInstance();
      string dbfilename = Path.Combine(ctx.dbDir, "history.sqlite3");
      SqliteConnectionStringBuilder ds = new SqliteConnectionStringBuilder {
        DataSource = dbfilename,
        Mode = SqliteOpenMode.ReadWriteCreate
      };
      conn = new SqliteConnection(ds.ToString());
      if (conn != null) {
        try {
          conn.Open();
          SqliteCommand cmd = conn.CreateCommand();
          cmd.CommandText = @"CREATE TABLE IF NOT EXISTS history(
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            timestamp INTEGER DEFAULT 0,
            summary TEXT NOT NULL DEFAULT 'unknown',
            icon TEXT NOT NULL DEFAULT 'unknown',
            temperature REAL NOT NULL DEFAULT 0.0,
            feelslike REAL NOT NULL DEFAULT 0.0,
            dewpoint REAL DEFAULT 0.0,
            windbearing INTEGER DEFAULT 0,
            windspeed REAL DEFAULT 0.0,
            windgust REAL DEFAULT 0.0,
            humidity REAL DEFAULT 0.0,
            visibility REAL DEFAULT 0.0,
            pressure REAL DEFAULT 1013.0,
            precip_probability REAL DEFAULT 0.0,
            precip_intensity REAL DEFAULT 0.0,
            precip_type TEXT DEFAULT 'none',
            cloudCover REAL DEFAULT 0.0,
            cloudBase REAL DEFAULT 0.0,
            cloudCeiling REAL DEFAULT 0.0,
            moonPhase INTEGER DEFAULT 0,
            uvindex INTEGER DEFAULT 0,
            sunrise INTEGER DEFAULT 0,
            sunset INTEGER DEFAULT 0,
            tempMax REAL DEFAULT 0.0,
            tempMin REAL DEFAULT 0.0,
            api TEXT NOT NULL DEFAULT 'unknown')";
          cmd.ExecuteNonQuery();
        } catch (SqliteException e) {
          Log.Logger.Warning(e.Message);
          conn.Close();
        } finally {
          conn.Close();
        }
      }
    }

    /**
     * <summary>add a data point to the history database
     * Method <c>AddRecord</c> adds a DataPoint record
     * </summary>
     * <param>  p: DataPoint</param>
     */
    internal void AddRecord(Data.DataPoint p)
    {
      SqliteCommand cmd = conn.CreateCommand();
      cmd.CommandText = @"INSERT INTO history(timestamp, summary, icon, temperature,
          feelslike, dewpoint, windbearing, windspeed,
          windgust, humidity, visibility, pressure,
          precip_probability, precip_intensity, precip_type,
          uvindex, sunrise, sunset, cloudBase, cloudCover, cloudCeiling, moonPhase,
          tempMin, tempMax, api) VALUES(@timestamp,@summary,@icon,@temperature,
          @feelslike,@dewpoint,@windbearing,@windspeed,@windgust,@humidity,@visibility,
          @pressure,@precip_probability,@precip_intensity,@precip_type,@uvindex,@sunrise,
          @sunset,@cloudbase,@cloudcover,@cloudceiling,@moonPhase,@tempMin,@tempMax,@api)";

      cmd.Parameters.AddWithValue("@timestamp", Util.ToUnixEpoch(p.timeRecorded));
      cmd.Parameters.AddWithValue("@summary", p.conditionAsString);
      cmd.Parameters.AddWithValue("@icon", p.weatherSymbol);
      cmd.Parameters.AddWithValue("@temperature", p.temperature);
      cmd.Parameters.AddWithValue("@feelslike", p.temperatureApparent);
      cmd.Parameters.AddWithValue("@dewpoint", p.dewPoint);
      cmd.Parameters.AddWithValue("@windbearing", p.windDirection);
      cmd.Parameters.AddWithValue("@windspeed", p.windSpeed);
      cmd.Parameters.AddWithValue("@windgust", p.windGust);
      cmd.Parameters.AddWithValue("@humidity", p.humidity);
      cmd.Parameters.AddWithValue("@visibility", p.visibility);
      cmd.Parameters.AddWithValue("@pressure", p.pressureSeaLevel);
      cmd.Parameters.AddWithValue("@precip_probability", p.precipitationProbability);
      cmd.Parameters.AddWithValue("@precip_intensity", p.precipitationIntensity);
      cmd.Parameters.AddWithValue("@precip_type", p.precipitationTypeAsString);
      cmd.Parameters.AddWithValue("@uvindex", (int)p.uvIndex);
      cmd.Parameters.AddWithValue("@sunrise", Util.ToUnixEpoch(p.sunriseTime));
      cmd.Parameters.AddWithValue("@sunset", Util.ToUnixEpoch(p.sunsetTime));
      cmd.Parameters.AddWithValue("@cloudbase", p.cloudBase);
      cmd.Parameters.AddWithValue("@cloudcover", p.cloudCover);
      cmd.Parameters.AddWithValue("@cloudceiling", p.cloudCeiling);
      cmd.Parameters.AddWithValue("@moonPhase", p.moonPhase);
      cmd.Parameters.AddWithValue("@tempMin", p.temperatureMin);
      cmd.Parameters.AddWithValue("@tempMax", p.temperatureMax);
      cmd.Parameters.AddWithValue("@api", p.Api);
      conn.Open();
      cmd.ExecuteNonQuery();
      conn.Close();
    }

    public void Dispose()
    {
      if (conn != null) {
        conn.Close();
        GC.SuppressFinalize(this);
      }
      Log.Logger.Information("Disposing DB connection");
    }

    private readonly SqliteConnection    conn = default!;
  }
}