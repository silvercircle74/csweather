/**
 * Copyright (c) 2012-2024 Alex Vie (as@subspace.cc)
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
using Serilog;
using Serilog.Context;
using Tommy;
using Spectre.Console;

#pragma warning disable IDE0032

namespace fetchweather {

  public sealed class GlobalContext : IDisposable
  {
    private static readonly string _appName;

    static GlobalContext()
    {
      _appName = "csweather";
    }

    private GlobalContext(CSWeatherSettings settings = null)
    {
      _iniOk = false;
      if (settings != null) {
        this.Options = settings;
      }
      this.httpClient = new HttpClient();
      this._configDir = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData),
          _appName);
      this._dataDir = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),
          _appName);

      appVersion = System.Reflection.Assembly.GetEntryAssembly()?.GetName().Version?.ToString();
      this.cacheDir = Path.Combine(_dataDir, "json");
      this.dbDir = Path.Combine(_dataDir, "db");
      System.IO.Directory.CreateDirectory(_configDir);
      System.IO.Directory.CreateDirectory(_dataDir);
      System.IO.Directory.CreateDirectory(dbDir);
      System.IO.Directory.CreateDirectory(cacheDir);
      string logFilePath = Path.Combine(this._dataDir, "log.txt");
      Serilog.Log.Logger = new Serilog.LoggerConfiguration().MinimumLevel.Debug()
        .WriteTo.File(logFilePath, rollingInterval: Serilog.RollingInterval.Day,
            outputTemplate: "{Timestamp:yyyy-MM-dd HH:mm:ss.fff zzz} [{Level}] [{ctx}] {Message}{NewLine}{Exception}")
        .Enrich.FromLogContext()
        .CreateLogger();
      LogContext.PushProperty("ctx", "GlobalContext()");
      Log.Information("-----------------------------------------------------------------------------------------------------");
      Log.Information("Serilog: Logger initialized");
      Log.Information("CSWeather appVersion {0}", appVersion);
      if (Options.ConfigFile != "") {
        if (File.Exists(Options.ConfigFile)) {
          _iniFilename = Options.ConfigFile;
        }
      } else {
        _iniFilename = Path.Combine(_configDir, "csw.toml");
      }
      if (File.Exists(_iniFilename)) {
        using StreamReader reader = File.OpenText(_iniFilename);
        try {
          iniData = TOML.Parse(reader);
        } catch (TomlParseException e) {
          Log.Error("The config file at {0} is not in valid TOML format. Exiting.");
          System.Environment.Exit(-1);
          Console.WriteLine(e.Message);
        }
        iniData["General"]["firstRun"] = "no";
        _iniOk = true;
      } else {
        /* this creates the default entries for all known APIs */
        _iniOk = false;
        iniData = new TomlTable();
        iniData["General"]["firstRun"] = "yes";
        Defaults.ProviderInfo[] pi = Defaults.getKnownApis();
        foreach(Defaults.ProviderInfo p in pi) {
          iniData[p.Name]["baseurl"] = p.Url;
          if (p.FCUrl != "") {
            iniData[p.Name]["fcurl"] = p.FCUrl;
          }
          iniData[p.Name]["location"] = "** Edit this **";
          iniData[p.Name]["apikey"] = "** Edit this **";
        }
        using StreamWriter writer = File.CreateText(_iniFilename);
        iniData.WriteTo(writer);
        writer.Flush();
      }
      CallStats = new AppStats(Path.Combine(_dataDir, "stats.toml"));
    }

    /**
     * <summary>
     * Just print a warning when the config was freshly created, because it is
     * likely invalid, missing important information.
     * </summary>
     * <returns>true if config file was just created</returns>
     */
    public bool CheckConfig()
    {
      if (_iniOk == false) {
        Console.WriteLine("The config file at {0} did not exist.", _iniFilename);
        Console.WriteLine("A default was created, but you must fill in your important data.");
        Console.WriteLine("Otherwise nothing will work. You can also submit vital settings");
        Console.WriteLine("on the command line. See the --help option.");
      }
      return _iniOk;
    }

    public void Dispose()
    {
      CallStats.Dispose();
      httpClient.Dispose();
      Serilog.Context.LogContext.PushProperty("ctx", "GlobalContext:Dispose()");
      Log.Information("Disposing global context, flushing logs");
      Log.CloseAndFlush();
      if (_iniOk) {
        using StreamWriter writer = File.CreateText(_iniFilename);
        iniData.WriteTo(writer);
        writer.Flush();
      }
    }

    public void DumpDebugInfo()
    {
      AnsiConsole.MarkupLine("[yellow bold]Application context:[/]");
      AnsiConsole.MarkupLine("[green]Data directory:     [/]{0}", _dataDir);
      AnsiConsole.MarkupLine("[green]Config directory:   [/]{0}", _configDir);
      AnsiConsole.MarkupLine("[green]Configuration file: [/]{0}", _iniFilename);
      AnsiConsole.MarkupLine("[green]Database directory: [/]{0}", dbDir);
      AnsiConsole.MarkupLine("[green]Cache directory:    [/]{0}", cacheDir);
      AnsiConsole.MarkupLine("[green]Application stats:  [/]{0}", CallStats.IniFilename);

      AnsiConsole.MarkupLine("[yellow bold]Supported Providers (for --api option):[/]");
      Defaults.ProviderInfo[] pi = Defaults.getKnownApis();
      foreach (Defaults.ProviderInfo p in pi) {
        AnsiConsole.MarkupLine("[green]{0}:[/]\t{1}", p.Name, p.Provider);
      }
    }
    private static string               appVersion;
    private static GlobalContext        s_instance = default!;
    private readonly string             _configDir;
    private readonly string             _dataDir;
    public string                       cacheDir { get; private set; }
    public string                       dbDir { get; private set; }
    private readonly string             _iniFilename;
    public CSWeatherSettings            Options { get; set; }
    public HttpClient                   httpClient { get; private set; }
    public AppStats                     CallStats { get; private set; }
    private readonly bool               _iniOk;
    public TomlTable                    iniData { get; private set; }

    public static GlobalContext         GetInstance(CSWeatherSettings settings = null)
    {
      s_instance ??= new GlobalContext(settings);
      return s_instance;
    }

    /**
     * keep track of stats, API calls per month, overall, transferred bytes
     * use a TOML file to store the stats
     */
    public class AppStats
    {
      public AppStats(string filename)
      {
        try {
          using StreamReader reader = File.OpenText(filename);
          _iniData = TOML.Parse(reader);
        } catch (Exception e) when (e is IOException or TomlParseException) {
          _iniData = new TomlTable();
          _iniData["general"]["runs"] = 0;
          using StreamWriter writer = File.CreateText(filename);
          _iniData.WriteTo(writer);
          writer.Flush();
        }
        int runs = int.Parse(_iniData["general"]["runs"]) + 1;
        _iniFilename = filename;
        _iniData["general"]["runs"] = runs;

        _now = DateTime.Now;
        _iniData["current"]["month"] = _now.Month;
        _iniData["current"]["year"] = _now.Year;
      }

      private readonly  TomlTable               _iniData;
      private readonly  string                  _iniFilename;
      private readonly  DateTime                _now;

      public string IniFilename => _iniFilename;
      /**
       * <summary>
       * update call statistics for the given provider by amount
       * </summary>
       * <param name="apiProvider">the API provider tag</param>
       * <param name="amount">number of calls to add</param>
       * <param name="bytes">number of bytes transferred (payload only)</param>
       */
      public void updateStats(string apiProvider, int amount = 1, long bytes = 0)
      {
        int overallCalls = _iniData[apiProvider]["overallCalls"].IsInteger ? _iniData[apiProvider]["overallCalls"] : 0;
        int monthlyCalls = _iniData[apiProvider]["monthlyCalls"].IsInteger ? _iniData[apiProvider]["monthlyCalls"] : 0;

        int currentMonth = _iniData[apiProvider]["currentMonth"].IsInteger ? _iniData[apiProvider]["currentMonth"] : 0;

        if (currentMonth == 0 || currentMonth != this._now.Month) {
          monthlyCalls = 0;
        }
        overallCalls += amount;
        monthlyCalls += amount;

        _iniData[apiProvider]["currentMonth"] = this._now.Month;
        _iniData[apiProvider]["monthlyCalls"] = monthlyCalls;
        _iniData[apiProvider]["overallCalls"] = overallCalls;
        _iniData[apiProvider]["lastCall"] = this._now;

        if (bytes > 0) {
          long overallBytes = _iniData[apiProvider]["overallBytes"].IsInteger ? _iniData[apiProvider]["overallBytes"] : 0;
          long monthlyBytes = _iniData[apiProvider]["monthlyBytes"].IsInteger ? _iniData[apiProvider]["monthlyBytes"] : 0;
          if (currentMonth == 0 || currentMonth != this._now.Month) {
            monthlyBytes = 0;
          }
          overallBytes += bytes;
          monthlyBytes += bytes;
          _iniData[apiProvider]["monthlyBytes"] = monthlyBytes;
          _iniData[apiProvider]["overallBytes"] = overallBytes;
        }
      }

      /**
       * <summary>
       * Show the stats for any given (or all) API providers
       * </summary>
       * <param name="provider">The provider (short)</param>
       */
      public void Show(string provider = "")
      {
        Defaults.ProviderInfo[] pi = Defaults.getKnownApis();
        foreach (Defaults.ProviderInfo p in pi) {
          if (p.Name == provider || provider == "") {
            ShowStatsFor(p);
          }
        }
      }

      /**
       * <summary>
       * Show the stats for a single provider
       * </summary>
       * <param name="p">ProviderInfo</param>
       */
      private void ShowStatsFor(Defaults.ProviderInfo p)
      {
        if (p != null && _iniData.TryGetNode(p.Name, out TomlNode node)) {
          AnsiConsole.MarkupLine("[yellow bold]Statistics for provider {0} ({1}):[/]", p.Name, p.Provider);
          AnsiConsole.MarkupLine("[green]Total calls:     [/]  {0}", _iniData[p.Name]["overallCalls"]);
          AnsiConsole.MarkupLine("[green]Calls this month:[/]  {0}", _iniData[p.Name]["monthlyCalls"]);
          AnsiConsole.MarkupLine("[green]Total Bytes:     [/]  {0:N2} kB", _iniData[p.Name]["overallBytes"] / 1024);
          AnsiConsole.MarkupLine("[green]Bytes this month:[/]  {0:N2} kB", _iniData[p.Name]["monthlyBytes"] / 1024);
          DateTime dt = DateTime.Parse(_iniData[p.Name]["lastCall"]);
          AnsiConsole.MarkupLine("[green]API last used:   [/]  {0}", dt);
        }
      }

      public void Dispose()
      {
        using StreamWriter writer = File.CreateText(_iniFilename);
        _iniData.WriteTo(writer);
        writer.Flush();
      }
    }
  }
}