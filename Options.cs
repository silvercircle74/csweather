/**
 * Copyright (c) 2012-2024 Alex Vie (as@subspace.cc)
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
using Spectre.Console.Cli;
using System.ComponentModel;

namespace fetchweather
{
  /**
   * <summary>
   * Implements the command line options using System.Console.Cli. Provide
   * reasonable defaults.
   * </summary>
   */
  public sealed class CSWeatherSettings : CommandSettings
  {
    [Description("Show API call statistics")]
    [CommandArgument(0, "[showstats]")]
    public string Showstats     { get; init; }

    [Description("The API key")]
    [CommandOption("--apikey|-k")]
    [DefaultValue("nokey")]
    public string ApiKey        { get; init; }

    [Description("The API Provider short name. can be OWM, CC, or VC")]
    [CommandOption("--api|-a")]
    [DefaultValue("OWN")]
    public string ApiProvider   { get; init; }

    [Description("Stay offline, try to retrieve cached data when available")]
    [CommandOption("-o|--offline")]
    [DefaultValue(false)]
    public bool   Offline       { get; init; }

    [Description("Use the Metric system for temperatures. Set to false to use imperial units")]
    [CommandOption("--metric|-m")]
    [DefaultValue(true)]
    public bool   Metric        { get; init; }

    [Description("Unit for visibility. Can be m, km or mi")]
    [CommandOption("--visunit|--vis-unit")]
    [DefaultValue("km")]
    public string VisUnit       { get; init; }

    [Description("Unit for air pressure. Use hPa or inhg")]
    [CommandOption("--pressure-unit|--pressure")]
    [DefaultValue("hPa")]
    public string PressureUnit  { get; init; }

    [Description("Unit for wind speed. Can be m/s, km/h, mph or knots")]
    [CommandOption("--wind-unit")]
    [DefaultValue("m/s")]
    public string WindUnit      { get; init; }

    [Description("Do not create database entry")]
    [CommandOption("--skipdb|--skip-db")]
    [DefaultValue(false)]
    public bool   SkipDb        { get; init; }

    [Description("Skip caching results.")]
    [CommandOption("--skip-cache")]
    [DefaultValue(false)]
    public bool   SkipCache     { get; init; }

    [Description("Your location. Must be in API specific format. Check the docs. Normally, it is lat,lon.")]
    [CommandOption("--loc|-l")]
    [DefaultValue("none")]
    public string location      { get; init; }

    [Description("Try Cache when online operation fails.")]
    [CommandOption("--try-cache|--trycache")]
    [DefaultValue(false)]
    public bool   TryCache      { get; init; }

    [Description("Debug. Print options and exit without doing anything.")]
    [CommandOption("--debug|-d")]
    [DefaultValue(false)]
    public bool   Debug         { get; init; }

    [Description("Dump output in JSON format to this file. Will be overwritten.")]
    [CommandOption("--dump-to|--dumpto")]
    [DefaultValue("")]
    public string dumpTo        { get; init; }

    [Description("Format for the dump, one of plain, json or xml")]
    [CommandOption("--dump-format|--dumpformat")]
    [DefaultValue("json")]
    public string DumpFormat    { get; init; }

    [Description("Silent mode, do not write anything to stdout")]
    [CommandOption("--silent|-s")]
    [DefaultValue(false)]
    public bool SilentMode      { get; init; }

    [Description("Use this configuration file instead of the default. Must exist with read access.")]
    [CommandOption("--config|-c")]
    [DefaultValue("")]
    public string ConfigFile      { get; init; }
  }
}