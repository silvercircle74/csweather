﻿/**
 * Copyright (c) 2012-2024 Alex Vie (as@subspace.cc)
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

using System.Text.Json;
using System.Text.Json.Serialization;
using Serilog;
using Serilog.Context;
using Spectre.Console;
using Spectre.Console.Cli;

namespace fetchweather
{
  /**
   * <summary>
   * Helper class for JSON serialization. Uses SourceGeneration to make it AOT
   * compatible.
   * </summary>
   */
  [JsonSourceGenerationOptions(GenerationMode = JsonSourceGenerationMode.Serialization)]
  [JsonSerializable(typeof(CSWeatherSettings))]
  internal partial class CSWeatherSettingsContext : JsonSerializerContext { }

  public class FetchWeatherApp
  {
    public static async Task<int> Main(string[] args)
    {
      CommandApp<CSWeatherCommand> app = new();
      app.Configure(static config => {
        config.SetApplicationName("CSWeather");
      });
      return await app.RunAsync(args);
    }
  }

  /**
   * <summary>
   * Implements the Command object with an associated CSWeatherSettings object
   * </summary>
   */
  public sealed class CSWeatherCommand : AsyncCommand<CSWeatherSettings>
  {
    public CSWeatherCommand() : base()
    {
      this.jso = new() {
        WriteIndented = true,
        TypeInfoResolver = CSWeatherSettingsContext.Default
      };
    }
    /**
     * <summary>
     * This is the real entry point where the work starts.
     * </summary>
     * <param name="context">The app context</param>
     * <param name="settings">The settings object, already populated with all
     * command line options</param>
     * <returns>exit code.</returns>
     */
    public override async Task<int> ExecuteAsync(CommandContext context, CSWeatherSettings settings)
    {
      Data.DataHandler d = null;
      AppDomain.CurrentDomain.ProcessExit += static (s, e) => {
        GlobalContext ctx = GlobalContext.GetInstance();
        ctx.Dispose();
      };
      GlobalContext ctx = GlobalContext.GetInstance(settings);
      /**
       * check whether we had found a valid configuration file. Exit with error
       * status if not.
       */
      if (!ctx.CheckConfig()) {
        return -1;
      }
      LogContext.PushProperty("ctx", "ExecuteAsync()");
      Log.Information("Application initialization successful. Starting with {0} API", ctx.Options.ApiProvider);
      d = ctx.Options.ApiProvider switch {
        "VC" => new Data.DataHandlerVC(),
        "CC" => new Data.DataHandlerCC(),
        "OWM" => new Data.DataHandlerOWM(),
        "OWM3" => new Data.DataHandlerOWM3(),
        _ => null
      };
      // --debug option just prints the command line options and some debugging
      // info and exit.
      if (settings.Debug) {
        Log.Information("Debug mode active. Print information and exit");
        AnsiConsole.MarkupLine("[yellow bold]Command line options (use --help for usage information):[/]");
        Console.WriteLine(JsonSerializer.Serialize(settings, this.jso));
        ctx.DumpDebugInfo();
        if (d == null) {
          AnsiConsole.MarkupLine("[red bold]No valid API provider found - no debug info.[/]");
        } else {
          d.DumpDebugInfo();
        }
        return 0;
      }
      if (settings.Showstats == "showstats") {
        ctx.CallStats.Show();
        return 0;
      }
      if (d != null) {
        if (await d.Fetch() == true && d.p.valid == true) {
          if (!ctx.Options.SilentMode) {
            d.doOutput(Console.Out);
          }
          d.doDump();
          Environment.Exit(0);
        } else {
          Log.Warning(string.Format("Fetch failed for {0} API", ctx.Options.ApiProvider));
          Environment.Exit(-1);
        }
      } else {
        AnsiConsole.MarkupLine("[red bold]No valid API provider found[/]");
        Log.Information("No valid API provider found. Exiting.");
        Environment.Exit(-1);
      }
      return 0;
    }

    private readonly JsonSerializerOptions jso;
  }
}